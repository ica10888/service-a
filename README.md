# 问题答案

### 问题一

``` bash 
docker build . -t ica10888/service-a:v0.0.1
```

在dockerhub上的镜像文件

![avatar](img/1.jpg)

### 问题二

执行 terraform 

``` bash 
curl -X POST http://127.0.0.1:8888/api/v1/tkeCluster -d '{"name":"test-1","vpc_id":"vpc-4s39are5","availability_zone":"ap-guangzhou-4","subnet_id":"subnet-541d6xtq","security_group_id":"sg-8dvl87xh"}'
```
在腾讯云账号上创建了一个 tke 集群

![avatar](img/2.jpg)

### 问题三

helm 部署

``` bash 
helm  install service-a --namespace service-wangweihan ./
```

![avatar](img/3.jpg)



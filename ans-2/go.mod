module gitlab/shreychen/service-a

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/urfave/cli/v2 v2.3.0
	gopkg.in/yaml.v2 v2.2.8
)

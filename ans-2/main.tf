terraform {
  required_providers {
    tencentcloud = {
      source = "tencentcloudstack/tencentcloud"
    }
  }
}

provider "tencentcloud" {
  secret_id  = "xxxxx-EXAMPLE"
  secret_key = "xxxxx-EXAMPLE"
  region     = "ap-guangzhou"
}
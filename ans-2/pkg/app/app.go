package app

import (
	"fmt"
	"net/http"
	"os/exec"

	"github.com/gin-gonic/gin"
)

type App struct {
	router *gin.Engine
	config *Config
}

func NewApp(conf *Config) *App {
	return &App{
		router: gin.Default(),
		config: conf,
	}
}

func (app *App) Run() {
	app.router.GET("/healthz", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status":  "OK",
			"message": "Service a is running well",
		})
	})
	app.router.POST("/api/v1/tkeCluster", func(c *gin.Context) {
		var body Tke_body
		err := c.ShouldBindJSON(&body)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  "BadRequest",
				"message": err.Error(),
			})
			return
		}
		//创建TF文件，每次覆盖
		CreateTFFile(body.Name, body.Vpc_id, body.Availability_zone, body.Subnet_id, body.Security_group_id)
		//terraform 二进制执行
		cmd := exec.Command("sh", "-c", "/root/terraform", "apply", "-auto-approve")
		b, err := cmd.Output()
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":      "BadRequest",
				"message":     string(b),
				"exit status": err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  "OK",
			"message": string(b),
		})
	})
	app.router.Run(fmt.Sprintf(":%s", app.config.Port))
}

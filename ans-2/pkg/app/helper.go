package app

import (
	"fmt"
	"io/ioutil"
	"os"
)

type Tke_body struct {
	Name              string `json:"name"`
	Vpc_id            string `json:"vpc_id"`
	Availability_zone string `json:"availability_zone"`
	Subnet_id         string `json:"subnet_id"`
	Security_group_id string `json:"security_group_id"`
}

func create_tke_template(name string, vpc_id string, availability_zone string, subnet_id string, security_group_id string) string {
	var tke_template = `# 创建 TKE 集群
resource "tencentcloud_kubernetes_cluster" "tke_test" {
  vpc_id                                     = "%s"
  cluster_version                            = "1.18.4"
  cluster_cidr                               = "172.16.0.0/16"
  cluster_max_pod_num                        = 64
  cluster_name                               = "%s"
  cluster_desc                               = "created by terraform"
  cluster_max_service_num                    = 2048
  cluster_internet                           = true
  managed_cluster_internet_security_policies = ["0.0.0.0/0"]
  cluster_deploy_type                        = "MANAGED_CLUSTER"
  cluster_os                                 = "tlinux2.4x86_64"
  container_runtime                          = "containerd"
  deletion_protection                        = false

  worker_config {
    instance_name              = "some-node"
    availability_zone          = "%s"
    instance_type              = "S5.MEDIUM2"
    system_disk_type           = "CLOUD_SSD"
    system_disk_size           = 50
    internet_charge_type       = "TRAFFIC_POSTPAID_BY_HOUR"
    internet_max_bandwidth_out = 1
    public_ip_assigned         = true
    subnet_id                  = "%s"
    security_group_ids         = ["%s"]
    enhanced_security_service = false
    enhanced_monitor_service  = false
    password                  = "Pass@123"
  }

}`
	return fmt.Sprintf(tke_template, vpc_id, name, availability_zone, subnet_id, security_group_id)
}

func CreateTFFile(name string, vpc_id string, availability_zone string, subnet_id string, security_group_id string) {
	createFileIfNotExist("tke.tf")
	writeFile("tke.tf", create_tke_template(name, vpc_id, availability_zone, subnet_id, security_group_id))
}

func writeFile(filename string, data string) {

	d1 := []byte(data)
	err := ioutil.WriteFile(filename, d1, 0644)
	if err != nil {
		panic("write file err : " + filename)
	}

}

func createFileIfNotExist(filename string) {
	createFile, er := os.Open(filename)
	defer func() {
		createFile.Close()
	}()
	if er != nil && os.IsNotExist(er) {
		os.Create(filename)
	}
}
